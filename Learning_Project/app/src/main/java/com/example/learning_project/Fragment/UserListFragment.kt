package com.example.learning_project.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.learning_project.Adapter.UserProfileAdapter
import com.example.learning_project.Model.ItemModels
import com.example.learning_project.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [UserListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class UserListFragment : Fragment(), UserProfileAdapter.UserActionterface {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var userProfileRecycleView: RecyclerView
    private var randomList = ArrayList<ItemModels> ()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        //set Defualt value
        for (i in 0..5) {
            val color = (Math.random()* 16777215).toInt() or (0xFF shl 24)

            randomList?.add(ItemModels(name = "Name $i", description = "Description $i", color = color, image = context?.getDrawable(R.drawable.egg), light = false))
        }


    }

    override fun onStart() {
        super.onStart()
        setRecycleView()
    }

    private fun setRecycleView() {
        val adapter = UserProfileAdapter(requireContext(),randomList)
        adapter.setListener(this)
        userProfileRecycleView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        userProfileRecycleView.adapter = adapter

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_user_list, container, false)
        initView(rootView)
        return rootView
    }

    private fun initView(rootView: View) {
        userProfileRecycleView = rootView.findViewById(R.id.userProfileRc)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment UserListFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            UserListFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(position: Int) {
        randomList[position].light= !randomList[position].light

        val x = randomList.filterNot{it.name==randomList[position].name}.forEach{
            it.light=false
        }

        userProfileRecycleView.adapter?.notifyDataSetChanged()
        Toast.makeText(
            requireContext(),
            "Success $position",
            Toast.LENGTH_LONG
        ).show()
    }
}