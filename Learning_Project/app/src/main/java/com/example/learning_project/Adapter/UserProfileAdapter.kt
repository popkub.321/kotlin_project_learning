package com.example.learning_project.Adapter

import android.content.Context
import android.graphics.Color
import android.location.GnssAntennaInfo.Listener
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.example.learning_project.Model.ItemModels
import com.example.learning_project.R
import kotlin.contracts.contract

class UserProfileAdapter (private var context : Context,private val dataList : ArrayList<ItemModels>) : RecyclerView.Adapter<UserProfileAdapter.ViewHolder>( ) {
    private  var actionListener : UserActionterface?=null

    interface UserActionterface{
        fun onClick(position: Int)
    }
    fun setListener(listener: UserActionterface){
        this.actionListener = listener
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_user_list, parent, false)
        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.nameTv.setText(dataList[position].name)
        holder.descriptionTv.setText(dataList[position].description)

        dataList[position].image.let {
            /*holder.profileImg.setImageDrawable(it)  // Set Image*/
            val url : String = "https://huaweiadvices.com/wp-content/uploads/2019/11/YouTube-APK-for-Huawei-Honor-download.jpg"
            Glide.with(context)
                .load(url).circleCrop()
                .into(holder.profileImg)


        }




        if (dataList[position].light){

            holder.profileCard.setCardBackgroundColor(context.getColor(R.color.purple_700))
        }else{
            holder.profileCard.setCardBackgroundColor(dataList[position].color)
        }

        holder.profileCard.setOnClickListener{
            actionListener?.onClick(position)

            /*holder.profileCard.*/
        }

    }

    override fun getItemCount(): Int {
        return 5
    }
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val profileImg: AppCompatImageView = itemView.findViewById(R.id.profileImg)
        val nameTv: TextView = itemView.findViewById(R.id.nameTv)
        val descriptionTv: TextView = itemView.findViewById(R.id.descriptionTv)
        val profileCard: CardView = itemView.findViewById(R.id.profileCard)
        val imageView: ImageView = itemView.findViewById(R.id.profileImg)





    }
}