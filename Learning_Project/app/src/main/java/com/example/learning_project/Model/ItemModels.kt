package com.example.learning_project.Model

import android.graphics.drawable.Drawable

data class ItemModels(
    val name: String,
    val description: String,
    val color: Int,
    val image: Drawable?,
    var light: Boolean
)
